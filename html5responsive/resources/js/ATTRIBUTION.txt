The PhotoSwipe JavaScript source code is taken directly, unmodified, from the PhotoSwipe program made available at:

https://photoswipe.com/

The original work is Copyright © 2019 Dmitry Semenov.

These source files are being redistributed under the GNU GPLv3, as described in the LICENCE.txt file found in the html5responsive directory of this software work.