# digiKam HTML5 gallery theme

<p>This repository has moved to <a href="https://codeberg.org/Bobulous/digikam-html5-gallery-theme">Codeberg</a>. This GitLab version has been archived (read-only mode). To contribute to this project visit the <a href="https://codeberg.org/Bobulous/digikam-html5-gallery-theme">Codeberg repository</a>.</p>
