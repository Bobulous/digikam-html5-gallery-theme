# digiKam HTML5 gallery theme

A theme for the digiKam HTML photo gallery tool which produces responsive HTML5 using CSS3 and UTF-8 encoding, and offers a choice of several visual styles.

This theme is now built into the [digiKam 7.0.0 release](https://www.digikam.org/news/2020-07-19-7.0.0_release_announcement/).

## What is this?

[digiKam](https://www.digikam.org/) is a photo collection organiser available for Linux and Windows. (It is also available for Mac, though I have no way to test this on a Mac, so you'll be a pioneer if you experiment with it in Mac.)

One of digiKam's features is an HTML gallery tool which allows you to select images or collections of images (based on tags, saved searches, star ratings, etc) and generate a directory which contains images, thumbnails, HTML pages, and supporting stylesheets and scripts, resulting in a self-contained photo gallery which you can upload to a web server to show off your work.

The digiKam HTML gallery tool comes with several built-in themes, but it can also be augmented with additional themes.

This project contains a new theme which allows digiKam to generate a photo gallery which is responsive (so it should resize itself to display nicely whether shown on a smartphone screen or a desktop computer monitor) using HTML5 and CSS3. And the resulting pages use UTF-8 character encoding so that non-Latin characters can be displayed (as photo captions, etc).

This theme comes with a few visual styles which give the resulting gallery pages very different looks, described below.

## What visual styles are part of this theme?

Currently four visual styles are included, all very different in their appearance.

### Basic style

[See example gallery](https://www.bobulous.org.uk/gallery/digikam/Basic/index.html)

A simple style with no frills. "Jiggle" mode has no effect on the Basic style.

### Lightbox style

[See example gallery](https://www.bobulous.org.uk/gallery/digikam/Lightbox/index.html)

A style which gives the apparance of photographic slides being viewed on a [lightbox](https://commons.wikimedia.org/wiki/File:2015_04_08_009_Analogfilme.jpg), and strips of photographic negative film act as decorative trim. "Jiggle" mode causes the slides to be rotated a little bit, to give the impression that a busy photographer has slung them down onto the lightbox without time to line them up neatly.

### Feed style

[See example gallery](https://www.bobulous.org.uk/gallery/digikam/Feed/index.html)

Inspired by social media feeds, this style creates a vertical column of image thumbnails with dates and caption text. "Jiggle" currently has no effect on the Feed style.

### Brown Card style

[See example gallery](https://www.bobulous.org.uk/gallery/digikam/BrownCard/index.html)

Based on an [old photo album](https://commons.wikimedia.org/wiki/File:Ledure_album_vol_a_voile_08.jpg) of brown card, with photo corners and rough-edged photo cards. By default thumbnails are shown in sepia tones, but "Jiggle" mode causes the thumbnails to use a mix of different levels of sepia, and greyscale.

## How do I use this?

**The digiKam team have now included this Html5 Responsive theme in digiKam 7.0.0!** This means that if you're using digiKam 7 or newer, you don't need to do anything to install this theme, and it should be built-in and ready to use. (If you're still using digiKam 6 then see the section further down this page for advice on installing this theme.)

Once you have digiKam 7 installed, and you've got a number of images or collections ready which you want to create an HTML gallery to show off, then go to "Tools"->"Create Html gallery..." and the gallery creator should appear. Select either "Images" (which will create a gallery containing the images you had highlighted before starting the tool) or "Albums" (which will let you select collections based on folders, tags, saved searches, ratings, labels, etc) then click "Next".

You should see an entry named "HTML5 Responsive" appear in the list of available themes. Select it and click "Next", and then choose your preferred gallery options, described below.

## Gallery options

Once you've selected the "HTML5 Responsive" theme you'll be presented with the "Theme Parameters" page which contains options to customise your gallery pages.

### Author
Enter the name you want to appear in the copyright notice at the bottom of each page. Leave this empty if you don't want a copyright notice to be shown.

### Style
Select one of the visual styles (described earlier on this page) to be applied to your gallery pages.

### Images per page
Sets the number of thumbnails which appear on each image list page.

### Page links location
Select "Both" to have a pagination line (with links to previous and next pages) appear at the top **and** bottom of each image list page. Select "Bottom" if you only want a pagination line to appear at the end of each image list page. Select "Top" if you only want a pagination line to appear at the start of each image list page.

### Top pagination mode
Select "Current" if you want the top pagination line to show just the current page number and the total page count, such as "1 / 8". Select "List all" if you want links to all page numbers to be shown, such as "1 2 3 4 5 6 7 8". In both modes "previous" and "next" links will be shown.

### Bottom pagination mode
Same as the "Top pagination mode" but affects the bottom pagination line.

### Use PhotoSwipe
Select "Yes" to use Dmitry Semenov's [PhotoSwipe](https://photoswipe.com/) interactive gallery module. When PhotoSwipe is built into a gallery, clicking on a thumbnail on an image list page will cause PhotoSwipe to display that image, and it supports navigation using a mouse, a keyboard, and/or touch gestures, making it easy to browse quickly through the entire collection of images in each album regardless of whether the visitor is using a desktop PC or a smartphone. Select "No" if you don't want PhotoSwipe to be used, in which case clicking on an image list thumbnail will take you directly to the "Original Image" detail page.

### PhotoSwipe background opacity
Sets the opacity of the background around the image when using PhotoSwipe. Value 10 is fully opaque, and value 0 is fully transparent. Experiment with different values, but be aware that a low opacity can make it tricky to read the photo caption text in some situations where the background shines through too brightly.

### PhotoSwipe sharing button
Select "Yes" if you want a sharing button to appear in the top-right of the PhotoSwipe display. This contains links allowing users to share the current gallery page on popular social media sites. Select "No" if you don't need a sharing button to be shown.

### Add jiggle
Select "Yes" if you want some variety injected into the way thumbnails are presented on the image list pages. For example, when jiggle is used with the Lightbox style the thumbnails are displayed at assorted angles of rotation. Select "No" if you want uniformity on the image list pages.

### Show GPS data
Select "Yes" if you want the "Original Image" detail page to show GPS data in the "Exif data" table (where the image holds such data). Think carefully before creating a gallery with this set to "Yes", because you probably do not want to share precise coordinates of sensitive locations (such as someone's home address, or a military base) when you publish a photo gallery. Select "No" to exclude GPS data from the gallery.

Also be aware that digiKam remembers whatever your last settings were, so always check this GPS setting when creating a gallery, so that you don't accidentally add GPS data to a gallery without thinking about it.

## How do I install this for digiKam 6?

If for any reason you can't yet install digiKam 7, and are still using digiKam 6, then you can still use this Html5 Responsive theme by installing it yourself. To do this, download this repository from here on Codeberg (look for the download symbol to the right of the HTTPS/SSH download URL a little way down from the top of the [project main page](https://codeberg.org/Bobulous/digikam-html5-gallery-theme)) and extract its content so that you can see the directory named "html5responsive". You need to copy this into the themes directory of your local digiKam configuration directory.

On **Linux** place a copy of the html5responsive directory into the path:

`~/.local/share/digikam/themes/`

where `~` refers to your personal home directory.

On **Windows** place a copy of the html5responsive directory into the path which looks like this:

`C:\Users\Bob\AppData\Local\digikam\themes`

but replace `C:\Users\Bob\` with the path to your own user folder.

(If this digikam directory does not already contain a themes directory then you need to create it.)

Once that themes directory is in place, start digiKam and you should then see "HTML5 Responsive" listed as an option alongside the built-in HTML gallery themes. (See the section about "How do I use this?" if you've never used the digiKam HTML gallery tool before.)